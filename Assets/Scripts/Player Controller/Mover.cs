﻿using UnityEngine;
using UltEvents;

[System.Serializable] public class MoveUltEvent : UltEvent<float> { }

public class Mover : MonoBehaviour
{
    [SerializeField] private Rigidbody _movementBody = default;
    [SerializeField] private Aimer _aimer = default;
    [SerializeField] private float _movementSpeed = default;
    [SerializeField] private float _turnSpeed = default;
    [SerializeField] private float _runVelocityThreshold = default;

    [SerializeField] private UltEvent OnMoveStart = default;
    [SerializeField] private MoveUltEvent OnMoveStay = default;
    [SerializeField] private UltEvent OnMoveEnd = default;

    [SerializeField] private UltEvent OnWalkStay = default;
    [SerializeField] private UltEvent OnRunStay = default;


    private bool _isMovingPreviously = default;

    public Rigidbody MovementBody => _movementBody;

    private bool IsMoving => _aimer.CameraRelativeAimDirection.sqrMagnitude != 0;
    private bool StartedMoving => IsMoving && !_isMovingPreviously;
    private bool StayedMoving => IsMoving && _isMovingPreviously;
    private bool EndedMoving => !IsMoving && _isMovingPreviously;


    private bool IsRunning => CurrentMovementVector.magnitude >= _runVelocityThreshold;


    private Vector3 CurrentMovementVector => _aimer.CameraRelativeAimDirection * _movementSpeed;

    private void FixedUpdate()
    {
        Quaternion targetRotation = Quaternion.LookRotation(_aimer.NonZeroCameraRelativeAimDirection, Vector3.up);
        Quaternion lerpedRotation = Quaternion.Slerp(_movementBody.rotation, targetRotation, _turnSpeed * Time.fixedDeltaTime);
        _movementBody.MoveRotation(lerpedRotation);

        _movementBody.AddForce(CurrentMovementVector * Time.fixedDeltaTime, ForceMode.VelocityChange);

        UpdateMoveEvents();
        _isMovingPreviously = IsMoving;
    }

    private void UpdateMoveEvents()
    {
        if (StartedMoving)
            OnMoveStart?.Invoke();
        if (StayedMoving)
        {
            OnMoveStay?.Invoke(CurrentMovementVector.magnitude);
            if (IsRunning) OnRunStay?.Invoke();
            else OnWalkStay?.Invoke();
        }
        if (EndedMoving)
            OnMoveEnd?.Invoke();
    }

    public void AffectMovementSpeed(float speedPercent) => _movementSpeed *= speedPercent;
}
