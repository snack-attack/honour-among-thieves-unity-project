﻿using UnityEngine;

public class StunnerZone : StunnerBase
{
    [SerializeField] private float _rotationTime = default;
    [SerializeField] private Vector3 _stunPositionOffset = default;
    
    protected override float RotationTime => _rotationTime;

    public Vector3 GetStunPosition() => transform.position + _stunPositionOffset;

    private void OnTriggerEnter(Collider other)
    {
        Stunnable stunnable = other.gameObject.GetComponentRelative<Stunnable>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, true);
        Vector3 stunNormal = GetStunPosition() - other.transform.position;
        StoreStunNormal(stunNormal);
        Affect(stunnable);
    }
}
