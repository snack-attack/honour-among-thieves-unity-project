﻿using System.Collections.Generic;
using UltEvents;
using UnityEngine;



public class LootZone : MonoBehaviour
{
    [SerializeField] private List<LootInfo> _initialLootInfos = default;

    [SerializeField] private LootInfoUltEvent OnLootInfoAdded = default;
    [SerializeField] private LootUltEvent OnItemLooted = default;
    [SerializeField] private UltEvent OnAllLootCollected = default;

    private readonly Dictionary<LootInfo, int> _lootInfos = new Dictionary<LootInfo, int>();
    private readonly Dictionary<LootInfo, int> _lootedInfos = new Dictionary<LootInfo, int>();


    /// <summary> Call Loot Item added event for the items added in inspector. </summary>
    private void Awake()
    {
        foreach (LootInfo lootInfo in _initialLootInfos)
            AddLootInfo(lootInfo);
    }

    public void AddLootInfo(LootInfo lootInfo)
    {
        if (lootInfo == null)
            return;

        if (!_lootInfos.ContainsKey(lootInfo))
            _lootInfos.Add(lootInfo, 0);

        _lootInfos[lootInfo]++;
        OnLootInfoAdded?.Invoke(lootInfo);
    }


    private void OnTriggerEnter(Collider other)
    {
        LootItem lootItem = other.gameObject.GetComponentRelative<LootItem>(eComponentRelativeType.SELF | eComponentRelativeType.PARENT, true);
        if (lootItem == null)
            return;

        if (CanClaimLootInfo(lootItem.LootInfo))
            ClaimLootItem(lootItem);
    }


    private bool CanClaimLootInfo(LootInfo lootInfo)
    {
        if (_lootInfos.ContainsKey(lootInfo))
        {
            if (!_lootedInfos.ContainsKey(lootInfo))
                return true;

            if (_lootedInfos[lootInfo] < _lootInfos[lootInfo])
                return true;
        }

        return false;
    }


    private void ClaimLootItem(LootItem lootItem)
    {
        if (!_lootedInfos.ContainsKey(lootItem.LootInfo))
            _lootedInfos.Add(lootItem.LootInfo, 0);

        _lootedInfos[lootItem.LootInfo]++;
        OnItemLooted?.Invoke(lootItem.LootInfo, lootItem.transform.position);
        lootItem.gameObject.SetActive(false);

        CheckAllLootCollected();
    }

    private void CheckAllLootCollected()
    {
        foreach (LootInfo lootInfo in _lootInfos.Keys)
            if (!_lootedInfos.ContainsKey(lootInfo) || _lootedInfos[lootInfo] < _lootInfos[lootInfo])
                return;

        OnAllLootCollected?.Invoke();
        PackLoot();
    }

    private void PackLoot() => gameObject.SetActive(false);
}