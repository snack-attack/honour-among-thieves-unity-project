﻿using System;
using System.Linq;


public static class ArrayExtensions
{
    public static T[] SubArray<T>(this T[] source, int offset, int length)
    {
        return source.Skip(offset)
                        .Take(length)
                        .ToArray();
    }

    public static T[] Shuffled<T>(this T[] source)
    {
        Random rnd = new Random();
        return source.OrderBy(x => rnd.Next()).ToArray();
    }

    public static T GetRandom<T>(this T[] source) => source[new Random().Next(0, source.Length)];
}