﻿using UnityEngine;
using UnityEngine.InputSystem;


public class Aimer : MonoBehaviour
{
    private Camera _mainCamera = default;

    public Vector3 CameraRelativeAimDirection { get; private set; } = default;
    public Vector3 NonZeroCameraRelativeAimDirection { get; private set; } = Vector3.back;


    private void Awake() => _mainCamera = Camera.main;

    private void OnMove(InputValue value)
    {
        Vector2 inputDirection = value.Get<Vector2>();
        Vector3 inputDirection3D = new Vector3(inputDirection.x, 0.0f, inputDirection.y);

        CameraRelativeAimDirection = _mainCamera.transform.TransformDirection(inputDirection3D);
        CameraRelativeAimDirection = new Vector3(CameraRelativeAimDirection.x, 0.0f, CameraRelativeAimDirection.z);
        CameraRelativeAimDirection = CameraRelativeAimDirection.normalized;
        CameraRelativeAimDirection *= inputDirection.magnitude;

        if (CameraRelativeAimDirection.sqrMagnitude > 0.0f)
            NonZeroCameraRelativeAimDirection = CameraRelativeAimDirection;
    }
}
