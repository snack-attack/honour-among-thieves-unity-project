﻿using UnityEngine;


public abstract class Affector<T> : MonoBehaviour where T : Component
{
    protected abstract void Affect(T affected);
}
