﻿using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName="SnackAttack/LootPool")]
public class LootPool : ScriptableObject
{
	[SerializeField] private LootPoolItem[] _lootPoolItems = default;


	public void GetInstantiatedLootLists(int lootCount, int listCount, List<List<LootItem>> lootLists)
	{
		if (lootCount * listCount > _lootPoolItems.Length)
			Debug.LogError("Not enough items to create loot lists!", this);

		List<Vector3> takenPositions = new List<Vector3>(lootCount * listCount);
		for (int i = 0; i < listCount; ++i)
		{
			List<LootItem> randomItems = GetInstantiatedAndShuffledLootItems(lootCount, _lootPoolItems, takenPositions);
			lootLists.Add(randomItems);
		}
	}

	private static List<LootItem> GetInstantiatedAndShuffledLootItems(int length, LootPoolItem[] lootPoolItems, List<Vector3> takenPositions)
	{
		List<LootItem> outputItems = new List<LootItem>(length);

		LootPoolItem[] shuffledLootPoolItem = lootPoolItems.Shuffled();
		LootPoolItem[] trimmedLootPoolItems = shuffledLootPoolItem.SubArray(0, length);

		foreach (LootPoolItem lootPoolItem in trimmedLootPoolItems)
		{
			LootItem spawnedLootItem = SpawnLootItem(lootPoolItem, takenPositions);
			if (spawnedLootItem != null)
				outputItems.Add(spawnedLootItem);
		}

		return outputItems;
	}

	private static LootItem SpawnLootItem(LootPoolItem lootPoolItem, List<Vector3> takenPositions)
	{
		Vector3 safeSpawnPosition = GetSafeSpawnPosition(lootPoolItem, takenPositions);
		GameObject spawnedObject = Instantiate(lootPoolItem.LootObject, safeSpawnPosition, Quaternion.identity);
		LootItem spawnedLootItem = spawnedObject.GetComponentRelative<LootItem>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, true);
		if (spawnedLootItem == null)
		{
			Debug.LogError("Loot Pool Item does not contain a LootItem component!", lootPoolItem.LootObject);
			return null;
		}
		return spawnedLootItem;
	}

	private static Vector3 GetSafeSpawnPosition(LootPoolItem lootPoolItem, List<Vector3> takenPositions)
	{
		int spawnPositionIndex = Random.Range(0, lootPoolItem.LootPositions.Length);
		Vector3 spawnPosition = Vector3.zero;
		for (int i = 0; i < lootPoolItem.LootPositions.Length; ++i)
		{
			spawnPosition = lootPoolItem.LootPositions[spawnPositionIndex];
			if (!takenPositions.Exists((pos) => pos == spawnPosition))
				break;

			spawnPositionIndex = (spawnPositionIndex + 1) % lootPoolItem.LootPositions.Length;
		}

		if (!takenPositions.Contains(spawnPosition))
			takenPositions.Add(spawnPosition);
		return spawnPosition;
	}
}