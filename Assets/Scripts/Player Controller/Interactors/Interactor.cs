﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public abstract class Interactor<T> : MonoBehaviour where T : MonoBehaviour, IInteractable
{
    private struct InteractInfo
    {
        public IInteractable interactable;
        public float interactDistance;


        public InteractInfo(IInteractable interactable, float interactDistance)
        {
            this.interactable = interactable;
            this.interactDistance = interactDistance;
        }
    }


    [SerializeField] private Transform _interactTransform = default;
    private T _highlightedInteractable = default;
    private readonly List<IInteractable> _overlappingInteractables = new List<IInteractable>();


    protected abstract void Interact(T interactable);
    protected abstract void Highlight(T interactable);
    protected abstract void UnHighlight(T interactable);




    private void OnTriggerEnter(Collider other)
    {
        IInteractable interactable = other.gameObject.GetComponentRelative<IInteractable>(eComponentRelativeType.SELF | eComponentRelativeType.PARENT | eComponentRelativeType.CHILDREN, false);
        if (interactable != null && !_overlappingInteractables.Contains(interactable))
            _overlappingInteractables.Add(interactable);
    }

    private void OnTriggerExit(Collider other)
    {
        IInteractable interactable = other.gameObject.GetComponentRelative<IInteractable>(eComponentRelativeType.SELF | eComponentRelativeType.PARENT | eComponentRelativeType.CHILDREN, false);
        if (interactable != null && _overlappingInteractables.Contains(interactable))
            _overlappingInteractables.Remove(interactable);
    }

    private void OnDisable()
    {
        FlushOverlappingInteractables();
        UpdateHighlightedInteractable();
    }


    public void FlushOverlappingInteractables() => _overlappingInteractables.Clear();


    private void OnInteract()
    {
        IInteractable closestInteractable;
        T closestT;
        FindClosestInteractableFromInteractables(_overlappingInteractables, out closestInteractable, out closestT);

        if (closestT != closestInteractable)
            return;

        OnInteract(closestT);
    }


    private void OnInteract(T interactable)
    {
        if (interactable != null)
            interactable.OnInteractEvent?.Invoke();

        Interact(interactable);
    }

    private void FixedUpdate() => UpdateHighlightedInteractable();

    private void UpdateHighlightedInteractable()
    {
        T closestT;
        FindClosestInteractableFromInteractables(_overlappingInteractables, out _, out closestT);


        if (closestT != _highlightedInteractable)
            OnUnHighlight(_highlightedInteractable);
        if (closestT != null && closestT != _highlightedInteractable)
            OnHighlight(closestT);

        _highlightedInteractable = closestT;
    }


    private void OnHighlight(T interactable)
    {
        interactable.OnHighlightEvent?.Invoke();
        interactable.OnChangeHighlightEvent?.Invoke(true);
        Highlight(interactable);
    }

    private void OnUnHighlight(T interactable)
    {
        if (interactable != null)
        {
            interactable.OnUnHighlightEvent?.Invoke();
            interactable.OnChangeHighlightEvent?.Invoke(false);
        }

        UnHighlight(interactable);
    }


    private void FindClosestInteractableFromInteractables(List<IInteractable> interactables, out IInteractable closestInteractable, out T closestT)
    {
        var interactInfos = from interactable in interactables
                            where interactable != null && interactable.InteractableCollider != null && interactable.CanInteract
                            let closestPoint = interactable.InteractableCollider.ClosestPoint(_interactTransform.position)
                            let interactDistance = Vector3.Distance(_interactTransform.position, closestPoint)
                            select new InteractInfo(interactable, interactDistance);


        InteractInfo closestInteractInfo = new InteractInfo(default, float.PositiveInfinity);
        foreach (InteractInfo interactInfo in interactInfos)
            if (interactInfo.interactDistance < closestInteractInfo.interactDistance)
                closestInteractInfo = interactInfo;


        closestT = closestInteractInfo.interactable as T;
        closestInteractable = closestInteractInfo.interactable;
    }
}
