﻿using UltEvents;
using UnityEngine;

[System.Serializable] public class BoolUltEvent : UltEvent<bool> { }

public interface IInteractable
{
    Collider InteractableCollider { get; }
    bool CanInteract { get; set; }

    UltEvent OnInteractEvent { get; }
    BoolUltEvent OnChangeHighlightEvent { get; }
    UltEvent OnHighlightEvent { get; }
    UltEvent OnUnHighlightEvent { get; }
}
