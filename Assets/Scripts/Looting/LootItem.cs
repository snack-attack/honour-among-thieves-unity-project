﻿using UnityEngine;



public class LootItem : MonoBehaviour
{
    [SerializeField] private LootInfo _lootInfo = default;
    public LootInfo LootInfo => _lootInfo;
}