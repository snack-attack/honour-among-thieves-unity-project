﻿using UltEvents;
using UnityEngine;


public class PressableSwitch : Pressable
{
    [SerializeField] private bool _initiallyActive = default;

    [SerializeField] private PressableSwitchUltEvent OnPressed = default;
    [SerializeField] private UltEvent OnActivate = default;
    [SerializeField] private UltEvent OnDeactivate = default;

    public bool Active { get; private set; } = true;


    private void Start() => Active = _initiallyActive;

    public override void Press()
    {
        Active = !Active;
        OnPressed?.Invoke(Active);

        if (Active) 
            OnActivate?.Invoke();
        else 
            OnDeactivate?.Invoke();
    }
}