﻿using UnityEngine;
using UltEvents;



public class CollisionFilter : MonoBehaviour
{
    [SerializeField] private GameObject[] _permittedObjects = default;

    [SerializeField] private TriggerEvent3D OnValidTrigger = default;
    [SerializeField] private TriggerEvent3D OnInvalidTrigger = default;

    [SerializeField] private CollisionEvent3D OnValidCollision = default;
    [SerializeField] private CollisionEvent3D OnInvalidCollision = default;


    private void OnTriggerEnter(Collider other)
    {
        if (IsObjectValid(other.gameObject))
            OnValidTrigger?.Invoke(other);
        else
            OnInvalidTrigger?.Invoke(other);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (IsObjectValid(collision.gameObject))
            OnValidCollision?.Invoke(collision);
        else
            OnInvalidCollision?.Invoke(collision);
    }

    private bool IsObjectValid(GameObject gameObject)
    {
        foreach (GameObject permittedObject in _permittedObjects)
            if (gameObject.transform.IsChildOf(permittedObject.transform))
                return true;
        return false;
    }
}
