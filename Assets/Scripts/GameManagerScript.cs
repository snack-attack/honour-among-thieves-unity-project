﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    static public GameManagerScript instance = null;

    public float maxTimer, tickSpeedMultiplier = 1f;
    float timer;

    public GameObject gameEnd;

    public Text clockText;
    public Image clockImg;

    bool ticking = true;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        timer = maxTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (ticking)
        {
            timer -= Time.deltaTime * tickSpeedMultiplier;

            clockText.text = ((int)timer).ToString();
            clockImg.fillAmount = timer / maxTimer;

            if (timer < 0f)
                EndGame();

        }
    }

    public void EndGame(int winningPlayerId = -1)
    {
        ticking = false;
        Time.timeScale = 0f;
        GameObject end = Instantiate(gameEnd);
        if(winningPlayerId >= 0)
        {
            Text text = end.GetComponentInChildren<Text>();
            text.text = "Player " + (winningPlayerId + 1) + " wins!";
        }
    }
}
