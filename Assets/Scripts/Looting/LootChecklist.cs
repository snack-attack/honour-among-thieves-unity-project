﻿using System.Collections.Generic;
using UnityEngine;


public class LootChecklist : MonoBehaviour
{
    [SerializeField] private GameObject _togglePrefab = default;
    private readonly List<LootToggle> _lootToggles = new List<LootToggle>();


    private void Awake()
    {
        LootToggle foundToggle = _togglePrefab.GetComponentRelative<LootToggle>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, false);
        if (foundToggle == null)
            Debug.LogError("Toggle Prefab for Loot Checklist does not contain a LootToggle component!", gameObject);
    }

    public void AddLootInfo(LootInfo lootInfo) => CreateLootToggle(lootInfo);

    private void CreateLootToggle(LootInfo lootInfo)
    {
        GameObject lootToggleObject = Instantiate(_togglePrefab, transform);
        LootToggle lootToggle = lootToggleObject.GetComponentRelative<LootToggle>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, false);
        lootToggle.SetLootInfo(lootInfo);
        _lootToggles.Add(lootToggle);
    }

    public void ClaimLootItem(LootInfo lootInfo)
    {
        LootToggle foundToggle = GetUntoggledLootToggleFromLootInfo(lootInfo);
        if (foundToggle != null)
            foundToggle.Toggle();
    }

    private LootToggle GetUntoggledLootToggleFromLootInfo(LootInfo lootInfo) => _lootToggles.Find((lt) => { return lt.LootInfo == lootInfo && !lt.Toggled; });
}