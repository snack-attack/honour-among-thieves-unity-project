﻿using UnityEngine;

public class Dasher : MonoBehaviour
{
    [SerializeField] private Rigidbody _dashBody = default;
    [SerializeField] private Aimer _aimer = default;
    [SerializeField] private float _dashStrength = default;

    [SerializeField] private DashUltEvent OnDashEvent = default;

    private int _dashDisablers = default;

    private bool CanDash => _dashDisablers <= 0;
    public Rigidbody DashBody => _dashBody;

    
    private void OnDash()
    {
        if (!CanDash)
            return;

        Vector3 dashDirection = _aimer.NonZeroCameraRelativeAimDirection.normalized;
        Vector3 dashForce = dashDirection * _dashStrength;
        _dashBody.AddForce(dashForce, ForceMode.Impulse);
        OnDashEvent?.Invoke(dashForce);
    }


    public void AffectDashStrength(float strengthPercent) => _dashStrength *= strengthPercent;
    public void EnableDash() => _dashDisablers -= 1;
    public void DisableDash() => _dashDisablers += 1;
}
