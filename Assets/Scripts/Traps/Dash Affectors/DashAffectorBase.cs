﻿using UnityEngine;


public class DashAffectorBase : ContinuousAffector<Dasher>
{
    [SerializeField] private float _dashStrengthPercent = default;

    [SerializeField] private DashDisablerUltEvent OnDashDisableEnter = default;
    [SerializeField] private DashDisablerUltEvent OnDashDisableStay = default;
    [SerializeField] private DashDisablerUltEvent OnDashDisableExit = default;


    protected override void OnAffectedAdded(Dasher dasher)
    {
        if (_dashStrengthPercent > 0)
            dasher.AffectDashStrength(_dashStrengthPercent);
        else
            dasher.DisableDash();

        OnDashDisableEnter?.Invoke(dasher.DashBody.position);
    }

    protected override void Affect(Dasher dasher)
    {
        OnDashDisableStay?.Invoke(dasher.DashBody.position);
    }

    protected override void OnAffectedRemoved(Dasher dasher)
    {
        if (_dashStrengthPercent > 0)
            dasher.AffectDashStrength(1 / _dashStrengthPercent);
        else
            dasher.EnableDash();

        OnDashDisableExit?.Invoke(dasher.DashBody.position);
    }
}
