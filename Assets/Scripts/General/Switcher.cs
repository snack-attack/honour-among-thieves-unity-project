﻿using UnityEngine;
using UltEvents;


[System.Serializable] public class SwitchUltEvent : UltEvent<bool> { }


public class Switcher : MonoBehaviour
{
    [SerializeField] private bool _startOn = true;

    [SerializeField] private SwitchUltEvent OnSwitch = default;
    [SerializeField] private UltEvent OnSwitchOn = default;
    [SerializeField] private UltEvent OnSwitchOff = default;

    public bool On { get; set; }
    public bool Off => !On;


    private void Awake() => On = _startOn;
    public bool Switch()
    {
        On = !On;
        OnSwitch?.Invoke(On);

        if (On) OnSwitchOn?.Invoke();
        else OnSwitchOff?.Invoke();

        return On;
    }

    public void SwitchOn()
    {
        if (Off)
            Switch();
    }

    public void SwitchOff()
    {
        if (On)
            Switch();
    }
}
