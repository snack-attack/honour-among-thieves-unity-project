﻿using UnityEngine;

public static class AudioController
{
    public static void PlaySFX(AudioClip audioClip, Transform transform, float volume) => AudioSource.PlayClipAtPoint(audioClip, transform.position, volume);
    public static void PlaySFX(AudioClip audioClip, Vector3 position, float volume) => AudioSource.PlayClipAtPoint(audioClip, position, volume);
}
