﻿using System.Collections;
using UltEvents;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelLoader : MonoBehaviour
{
    [SerializeField] private float _waitTime = default;

    [SerializeField] private UltEvent OnLevelLoadStart = default;
    [SerializeField] private UltEvent OnLevelLoadEnd = default;

    private bool _levelIsLoading = false;


    public void LoadLevelDelayed(string levelName)
    {
        if (!_levelIsLoading)
            StartCoroutine(LoadLevelDelayedRoutine(levelName));
    }

    public void LoadLevelDelayed(int levelBuildIndex)
    {
        if (!_levelIsLoading)
            StartCoroutine(LoadLevelDelayedRoutine(levelBuildIndex));
    }

    private IEnumerator LoadLevelDelayedRoutine(string levelName)
    {
        _levelIsLoading = true;

        OnLevelLoadStart?.Invoke();

        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(levelName);
        loadOperation.allowSceneActivation = false;

        TimeTracker waitTimer = new TimeTracker();
        waitTimer.Set(_waitTime);

        while (!waitTimer.Stopped || !loadOperation.IsFinishedLoading())
            yield return null;

        OnLevelLoadEnd?.Invoke();
        loadOperation.allowSceneActivation = true;
    }

    private IEnumerator LoadLevelDelayedRoutine(int levelBuildIndex)
    {
        _levelIsLoading = true;

        OnLevelLoadStart?.Invoke();

        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(levelBuildIndex);
        loadOperation.allowSceneActivation = false;

        yield return new WaitForSecondsRealtime(_waitTime);

        while (!loadOperation.IsFinishedLoading())
            yield return null;

        OnLevelLoadEnd?.Invoke();
        loadOperation.allowSceneActivation = true;
    }
}
