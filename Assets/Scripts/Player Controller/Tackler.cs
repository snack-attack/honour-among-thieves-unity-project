﻿using UltEvents;
using UnityEngine;


[System.Serializable] public class TackleUltEvent : UltEvent<float> { }


public class Tackler : MonoBehaviour
{
    [SerializeField] private Rigidbody _tackleBody = default;
    [SerializeField] private float _tackleDuration = default;
    [SerializeField] private float _tackleResetDelay = 0.5f;

    [SerializeField] private TackleUltEvent OnTackle = default;


    private float _latestTackleStartTime = default;
    private bool CanTackle => Time.time - _latestTackleStartTime <= _tackleResetDelay;


    public void StartTackle() => _latestTackleStartTime = Time.time;

    private void OnCollisionEnter(Collision collision)
    {
        Stunnable tackleTarget = collision.gameObject.GetComponentRelative<Stunnable>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, false);
        if (tackleTarget == null)
            return;

        if (CanTackle)
        {
            tackleTarget.StunAlongNormal(_tackleDuration, collision.GetAverageCollisionNormal(), 0.0f, eStunType.KNOCKBACK);
            OnTackle?.Invoke(_tackleBody.velocity.magnitude);
        }
    }
}
