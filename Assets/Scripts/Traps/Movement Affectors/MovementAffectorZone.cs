﻿using UnityEngine;


public class MovementAffectorZone : MovementAffectorBase
{
    private void OnTriggerEnter(Collider other)
    {
        Mover mover = other.gameObject.GetComponentRelative<Mover>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, true);
        base.AddAffected(mover);
    }

    private void OnTriggerExit(Collider other)
    {
        Mover mover = other.gameObject.GetComponentRelative<Mover>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, true);
        base.RemoveAffected(mover);
    }
}
