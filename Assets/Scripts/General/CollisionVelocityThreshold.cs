﻿using UltEvents;
using UnityEngine;


public enum eVelocityType { SELF, OTHER, RELATIVE };

public class CollisionVelocityThreshold : MonoBehaviour
{
    [SerializeField] private float _velocityThreshold = default;
    [SerializeField] private eVelocityType _velocityType = default;
    [SerializeField] private Rigidbody _myRigidbody = default;

    [SerializeField] private CollisionEvent3D OnCollideEnter = default;
    [SerializeField] private CollisionEvent3D OnCollideStay = default;
    [SerializeField] private CollisionEvent3D OnCollideExit = default;



    public void OnCollisionEnter(Collision collision)
    {
        if (IsCollisionVelocityValid(collision))
            OnCollideEnter?.Invoke(collision);
    }

    public void OnCollisionStay(Collision collision)
    {
        if (IsCollisionVelocityValid(collision))
            OnCollideStay?.Invoke(collision);
    }

    public void OnCollisionExit(Collision collision)
    {
        if (IsCollisionVelocityValid(collision))
            OnCollideExit?.Invoke(collision);
    }


    private bool IsCollisionVelocityValid(Collision collision)
    {
        if (_velocityType == eVelocityType.SELF && _myRigidbody != null)
            return _myRigidbody.velocity.magnitude >= _velocityThreshold;
        else if (_velocityType == eVelocityType.OTHER && collision.rigidbody != null)
            return collision.rigidbody.velocity.magnitude >= _velocityThreshold;
        else if (_velocityType == eVelocityType.RELATIVE)
            return collision.relativeVelocity.magnitude >= _velocityThreshold;
        return false;
    }
}
