%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Raccoon_Torso_AvatarMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: COG_JNT
    m_Weight: 1
  - m_Path: COG_JNT/pelvis_End
    m_Weight: 1
  - m_Path: COG_JNT/spine01
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/ear_L_base_JNT
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/ear_L_base_JNT/ear_L_JNT
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/ear_R_base_JNT
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/ear_R_base_JNT/ear_R_JNT
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/eye_L_JNT
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/eye_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/eyelid_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/eyelid_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/head_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/neck/head/jaw
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/index01_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/index01_L/index02_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/index01_L/index02_L/index_L_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/middle01_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/middle01_L/middle02_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/middle01_L/middle02_L/middle_L_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/pinky01_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/pinky01_L/pink02_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/pinky01_L/pink02_L/pinky_L_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/thumb01_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/thumb01_L/thumb02_L
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_L/elbow_L/wrist_L/hand_L/thumb01_L/thumb02_L/thumb_L_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/index01_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/index01_R/index02_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/index01_R/index02_R/index_R_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/middle01_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/middle01_R/middle02_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/middle01_R/middle02_R/middle_R_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/pinky01_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/pinky01_R/pink02_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/pinky01_R/pink02_R/pinky_R_END
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/thumb01_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/thumb01_R/thumb02_R
    m_Weight: 1
  - m_Path: COG_JNT/spine01/spine02/shoulder_R/elbow_R/wrist_R/hand_R/thumb01_R/thumb02_R/thumb_R_END
    m_Weight: 1
  - m_Path: COG_JNT/tail01
    m_Weight: 1
  - m_Path: COG_JNT/tail01/tail02
    m_Weight: 1
  - m_Path: COG_JNT/tail01/tail02/tail03
    m_Weight: 1
  - m_Path: COG_JNT/tail01/tail02/tail03/tail_END
    m_Weight: 1
  - m_Path: COG_JNT/thigh_L
    m_Weight: 0
  - m_Path: COG_JNT/thigh_L/knee_L
    m_Weight: 0
  - m_Path: COG_JNT/thigh_L/knee_L/foot_L
    m_Weight: 0
  - m_Path: COG_JNT/thigh_L/knee_L/foot_L/toe_L_JNT
    m_Weight: 0
  - m_Path: COG_JNT/thigh_R
    m_Weight: 0
  - m_Path: COG_JNT/thigh_R/knee_R
    m_Weight: 0
  - m_Path: COG_JNT/thigh_R/knee_R/foot_R
    m_Weight: 0
  - m_Path: COG_JNT/thigh_R/knee_R/foot_R/toe_R_JNT
    m_Weight: 0
  - m_Path: master_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_L_CTRL_GRP/ear01_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_L_CTRL_GRP/ear01_L_CTRL/ear02_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_L_CTRL_GRP/ear01_L_CTRL/ear02_L_CTRL_GRP/ear02_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_R_CTRL_GRP/ear01_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_R_CTRL_GRP/ear01_R_CTRL/ear02_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/ear01_R_CTRL_GRP/ear01_R_CTRL/ear02_R_CTRL_GRP/ear02_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyelid_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyelid_L_CTRL_GRP/eyelid_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyelid_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyelid_R_CTRL_GRP/eyelid_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyes_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyes_CTRL_GRP/eyes_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyes_CTRL_GRP/eyes_CTRL/EYE_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyes_CTRL_GRP/eyes_CTRL/EYE_L_CTRL_GRP/eye_L__CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyes_CTRL_GRP/eyes_CTRL/eye_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/eyes_CTRL_GRP/eyes_CTRL/eye_R_CTRL_GRP/eye_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/jaw_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/head_CTRL_GRP/head_CTRL/jaw_CTRL_GRP/jaw_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/index_L1_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/index_L1_CTRL_GRP/index_L1_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/index_L1_CTRL_GRP/index_L1_CTRL/index_L2_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/index_L1_CTRL_GRP/index_L1_CTRL/index_L2_CTRL_GRP/index_L2_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/middle1_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/middle1_L_CTRL_GRP/raccoonRig:rig:middle1
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/middle1_L_CTRL_GRP/raccoonRig:rig:middle1/middle2_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/middle1_L_CTRL_GRP/raccoonRig:rig:middle1/middle2_L_CTRL_GRP/raccoonRig:rig:middle2_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/pinky1_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/pinky1_L_CTRL_GRP/raccoonRig:rig:pinky1_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/pinky1_L_CTRL_GRP/raccoonRig:rig:pinky1_CTRL/pinky2_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/pinky1_L_CTRL_GRP/raccoonRig:rig:pinky1_CTRL/pinky2_L_CTRL_GRP/raccoonRig:rig:pinky2_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/thumb_L1_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/thumb_L1_CTRL_GRP/thumb_L1_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/thumb_L1_CTRL_GRP/thumb_L1_CTRL/thumb_L2_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_L_CTRL_GRP/shoulder_L_CTRL/elbow_L_CTRL_GRP/elbow_L_CTRL/wrist_L_CTRL_GRP/wrist_L_CTRL/thumb_L1_CTRL_GRP/thumb_L1_CTRL/thumb_L2_CTRL_GRP/thumb_L2_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/index_R1_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/index_R1_CTRL_GRP/index_R1_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/index_R1_CTRL_GRP/index_R1_CTRL/index_R2_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/index_R1_CTRL_GRP/index_R1_CTRL/index_R2_CTRL_GRP/index_R2_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/middle1_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/middle1_R_CTRL_GRP/raccoonRig:rig:middle1
      1
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/middle1_R_CTRL_GRP/raccoonRig:rig:middle1
      1/middle2_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/middle1_R_CTRL_GRP/raccoonRig:rig:middle1
      1/middle2_R_CTRL_GRP/raccoonRig:rig:middle2_CTRL 1
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/pinky1_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/pinky1_R_CTRL_GRP/raccoonRig:rig:pinky1_CTRL
      1
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/pinky1_R_CTRL_GRP/raccoonRig:rig:pinky1_CTRL
      1/pinky2_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/pinky1_R_CTRL_GRP/raccoonRig:rig:pinky1_CTRL
      1/pinky2_R_CTRL_GRP/raccoonRig:rig:pinky2_CTRL 1
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/thumb_R1_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/thumb_R1_CTRL_GRP/thumb_R1_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/thumb_R1_CTRL_GRP/thumb_R1_CTRL/thumb_R2_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/spine01_CTRL_GRP/spine01_CTRL/spine02_CTRL_GRP/spine02_CTRL/shoulder_R_CTRL_GRP/shoulder_R_CTRL/elbow_R_CTRL_GRP/elbow_R_CTRL/wrist_R_CTRL_GRP/wrist_R_CTRL/thumb_R1_CTRL_GRP/thumb_R1_CTRL/thumb_R2_CTRL_GRP/thumb_R2_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/tail1_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/tail1_CTRL_GRP/tail1_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/tail1_CTRL_GRP/tail1_CTRL/tail2_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/tail1_CTRL_GRP/tail1_CTRL/tail2_CTRL_GRP/tail2_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/tail1_CTRL_GRP/tail1_CTRL/tail2_CTRL_GRP/tail2_CTRL/tail3_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/COG_CTRL_GRP/COG_CTRL/tail1_CTRL_GRP/tail1_CTRL/tail2_CTRL_GRP/tail2_CTRL/tail3_CTRL_GRP/tail3_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_L_CTRL_GRP/foot_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_L_CTRL_GRP/foot_L_CTRL/toe_L_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_L_CTRL_GRP/foot_L_CTRL/toe_L_CTRL_GRP/toe_L_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_R_CTRL_GRP/foot_R_CTRL
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_R_CTRL_GRP/foot_R_CTRL/toe_R_CTRL_GRP
    m_Weight: 0
  - m_Path: master_CTRL_GRP/master_CTRL/foot_R_CTRL_GRP/foot_R_CTRL/toe_R_CTRL_GRP/toe_R_CTRL
    m_Weight: 0
  - m_Path: Raccoon_WHOLE
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/BODY_GRP
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/BODY_GRP/arms_low
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/BODY_GRP/eyes_low
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/BODY_GRP/legs_low
    m_Weight: 0
  - m_Path: Raccoon_WHOLE/BODY_GRP/neck_low
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/BODY_GRP/top_low
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/Raccoon_GRP
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/Raccoon_GRP/raccoonEyelid_low
    m_Weight: 0
  - m_Path: Raccoon_WHOLE/Raccoon_GRP/raccoonHead_low
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/Raccoon_GRP/raccoonNose_low
    m_Weight: 1
  - m_Path: Raccoon_WHOLE/Raccoon_GRP/raccoonTail_low
    m_Weight: 1
