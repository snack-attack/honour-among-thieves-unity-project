﻿using UnityEngine;


public class PusherZone : PusherBase
{
    private void OnTriggerEnter(Collider other) => base.Affect(other.attachedRigidbody);
}
