﻿using UnityEditor;
using UnityEngine;


#if UNITY_EDITOR

[CustomEditor(typeof(StunnerZone))]
public class StunnerZoneEditor : Editor
{
    private StunnerZone _targetObject = default;
    private SerializedProperty _stunPositionOffsetProperty = default;


    [DrawGizmo(GizmoType.Selected)]
    static void RenderCustomGizmo(StunnerZone stunnerZone, GizmoType gizmoType) => DrawStunPosition(stunnerZone.GetStunPosition());

    static void DrawStunPosition(Vector3 stunPosition)
    {
        Color originalGizmosColor = Gizmos.color;
        Gizmos.color = new Color(0f, 0f, 1.0f, 0.9f);
        Gizmos.DrawCube(stunPosition, Vector3.one);
        Gizmos.color = originalGizmosColor;
    }

    private void OnEnable()
    {
        _targetObject = target as StunnerZone;
        _stunPositionOffsetProperty = serializedObject.FindProperty("_stunPositionOffset");
    }

    private void OnSceneGUI()
    {
        if (_targetObject == null)
            return;

        serializedObject.Update();
        _stunPositionOffsetProperty.vector3Value = DoSpawnPositionHandle(_targetObject.GetStunPosition()) - _targetObject.transform.position;
        serializedObject.ApplyModifiedProperties();
    }

    private Vector3 DoSpawnPositionHandle(Vector3 stunPosition)
    {
        EditorGUI.BeginChangeCheck();
        Vector3 newStunPosition = Handles.PositionHandle(stunPosition, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(serializedObject.targetObject, "Change Stun Position");
            return newStunPosition;
        }

        return stunPosition;
    }
}

#endif