﻿using UnityEngine;


public class DashAffectorZone : DashAffectorBase
{
    private void OnTriggerEnter(Collider other)
    {
        Dasher dasher = other.gameObject.GetComponentRelative<Dasher>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, true);
        base.AddAffected(dasher);
    }
    private void OnTriggerExit(Collider other)
    {
        Dasher dasher = other.gameObject.GetComponentRelative<Dasher>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, true);
        base.RemoveAffected(dasher);
    }
}
