﻿using UnityEngine;

public static class AsyncOperationExtensions
{
    public static bool IsFinishedLoading(this AsyncOperation asyncOperation) => asyncOperation.progress >= 0.9f;
}