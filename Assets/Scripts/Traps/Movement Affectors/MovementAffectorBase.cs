﻿using UnityEngine;


public abstract class MovementAffectorBase : ContinuousAffector<Mover>
{
    [SerializeField] private float _movementSpeedPercent = default;

    [SerializeField] private SpeederUltEvent OnSpeedEnter = default;
    [SerializeField] private SpeederUltEvent OnSpeedStay = default;
    [SerializeField] private SpeederUltEvent OnSpeedExit = default;



    protected override void OnAffectedAdded(Mover mover)
    {
        mover.AffectMovementSpeed(_movementSpeedPercent);
        OnSpeedEnter?.Invoke(_movementSpeedPercent, mover.MovementBody.position);
    }

    protected override void Affect(Mover mover) => OnSpeedStay?.Invoke(_movementSpeedPercent, mover.MovementBody.position);

    protected override void OnAffectedRemoved(Mover mover)
    {
        mover.AffectMovementSpeed(1 / _movementSpeedPercent);
        OnSpeedExit?.Invoke(_movementSpeedPercent, mover.MovementBody.position);
    }
}
