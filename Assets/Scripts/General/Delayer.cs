﻿using UnityEngine;
using UltEvents;
using System.Collections;

public class Delayer : MonoBehaviour
{
    [SerializeField] private FloatUltEvent OnDelayStart = default;
    [SerializeField] private UltEvent OnDelayEnd = default;


    public void StartDelay(float duration) => StartCoroutine(DoDelay(duration));

    private IEnumerator DoDelay(float duration)
    {
        OnDelayStart?.Invoke(duration);
        yield return new WaitForSeconds(duration);
        OnDelayEnd?.Invoke();
    }
}
