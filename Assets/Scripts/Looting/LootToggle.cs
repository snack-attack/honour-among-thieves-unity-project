﻿using UnityEngine;
using UnityEngine.UI;


public class LootToggle : MonoBehaviour
{
    [SerializeField] private LootInfo _lootInfo = default;

    [SerializeField] private Image _lootImage = default;
    [SerializeField] private Image _toggleImage = default;

    public LootInfo LootInfo => _lootInfo;
    public bool Toggled { get; private set; }


    private void Awake()
    {
        SetLootInfo(_lootInfo);
        UnToggle();
    }

    public void Toggle()
    {
        _toggleImage.enabled = true;
        Toggled = true;
    }
    public void UnToggle()
    {
        _toggleImage.enabled = false;
        Toggled = false;
    }


    public void SetLootInfo(LootInfo lootInfo)
    {
        if (lootInfo == null)
        {
            _lootInfo = null;
            _lootImage.sprite = default;
        }
        else
        {
            _lootInfo = lootInfo;
            _lootImage.sprite = _lootInfo.LootSprite;
        }
    }
}
