﻿using System.Collections.Generic;
using UnityEngine;


public abstract class ContinuousAffector<T> : Affector<T> where T : Component
{
    private readonly List<T> _affectedList = new List<T>();


    protected void AddAffected(T affected)
    {
        if (affected == null)
            return;

        if (_affectedList.Contains(affected))
            return;

        _affectedList.Add(affected);
        OnAffectedAdded(affected);
    }

    protected abstract void OnAffectedAdded(T affected);


    private void FixedUpdate()
    {
        foreach (T affected in _affectedList.ToArray())
            Affect(affected);
    }

    protected void RemoveAffected(T affected)
    {
        if (affected == null)
            return;

        if (!_affectedList.Contains(affected))
            return;

        _affectedList.Remove(affected);
        OnAffectedRemoved(affected);
    }

    protected abstract void OnAffectedRemoved(T affected);

    private void OnDisable()
    {
        foreach (T affected in _affectedList.ToArray())
            RemoveAffected(affected);
    }
}
