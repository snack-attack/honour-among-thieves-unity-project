﻿using UnityEditor;
using UnityEngine;


#if UNITY_EDITOR

[CustomEditor(typeof(LootPool))]
public class LootPoolEditor : Editor
{
    private SerializedProperty _lootPoolItemsProperty = default;
    private int _selectedLootPoolItem = default;


    private void OnEnable()
    {
        _lootPoolItemsProperty = serializedObject.FindProperty("_lootPoolItems");
        SceneView.duringSceneGui += OnScene;
    }

    private void OnDisable() => SceneView.duringSceneGui -= OnScene;



    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        _selectedLootPoolItem = EditorGUILayout.IntSlider("Selected Loot Pool item", _selectedLootPoolItem, 0, _lootPoolItemsProperty.arraySize - 1);
        EditorGUILayout.PropertyField(_lootPoolItemsProperty);

        serializedObject.ApplyModifiedProperties();
    }


    private void OnScene(SceneView sceneView)
    {
        serializedObject.Update();

        SerializedProperty selectedLootPoolItem = _lootPoolItemsProperty.GetArrayElementAtIndex(_selectedLootPoolItem);
        DrawLootPoolItemPositionHandles(selectedLootPoolItem);
        DrawLootPoolItemPositionLabels(selectedLootPoolItem);

        serializedObject.ApplyModifiedProperties();
    }

    private void DrawLootPoolItemPositionHandles(SerializedProperty lootPoolItemProperty)
    {
        SerializedProperty lootPositionsProperty = lootPoolItemProperty.FindPropertyRelative("_lootPositions");

        for (int i = 0; i < lootPositionsProperty.arraySize; ++i)
        {
            EditorGUI.BeginChangeCheck();
            Vector3 lootPosition = lootPositionsProperty.GetArrayElementAtIndex(i).vector3Value;
            Vector3 newLootPosition = Handles.PositionHandle(lootPosition, Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(serializedObject.targetObject, "Change Loot Position");
                lootPositionsProperty.GetArrayElementAtIndex(i).vector3Value = newLootPosition;
            }
        }
    }

    private void DrawLootPoolItemPositionLabels(SerializedProperty lootPoolItemProperty)
    {
        SerializedProperty lootPositionsProperty = lootPoolItemProperty.FindPropertyRelative("_lootPositions");

        for (int i = 0; i < lootPositionsProperty.arraySize; ++i)
        {
            Vector3 lootPosition = lootPositionsProperty.GetArrayElementAtIndex(i).vector3Value;
            GUIStyle lootPositionLabelStyle = new GUIStyle();
            lootPositionLabelStyle.fontSize = 20;
            Handles.Label(lootPosition + new Vector3(0.1f, 0.5f, 0), i.ToString(), lootPositionLabelStyle);
        }
    }
}

#endif