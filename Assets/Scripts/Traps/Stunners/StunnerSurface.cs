﻿using UnityEngine;


public class StunnerSurface : StunnerBase
{
    [SerializeField] private float _velocityThreshold = default;
    protected override float RotationTime => 0.0f;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude < _velocityThreshold)
            return;

        Stunnable stunnable = collision.gameObject.GetComponentRelative<Stunnable>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, true);
        if (stunnable == null)
            return;

        StoreStunNormal(collision.GetAverageCollisionNormal());
        Affect(stunnable);
    }
}
