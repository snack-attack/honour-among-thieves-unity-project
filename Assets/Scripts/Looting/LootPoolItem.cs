﻿using System;
using UnityEngine;


[Serializable]
public struct LootPoolItem
{
	[SerializeField] private GameObject _lootObject;
	[SerializeField] private Vector3[] _lootPositions;

	public GameObject LootObject => _lootObject;
	public Vector3[] LootPositions => _lootPositions;


	public LootPoolItem(GameObject lootObject, Vector3[] lootPositions)
	{
		_lootObject = lootObject;
		_lootPositions = lootPositions;
	}
}