﻿using UnityEngine;


public class PressableInteractor : Interactor<Pressable>
{
    [SerializeField] private PressableInteractUltEvent OnInteract = default;
    [SerializeField] private PressableInteractUltEvent OnHighlight = default;
    [SerializeField] private PressableInteractUltEvent OnUnHighlight = default;


    protected override void Interact(Pressable interactable)
    {
        if (!interactable)
            return;

        interactable.Press();
        OnInteract?.Invoke(interactable);
    }

    protected override void Highlight(Pressable interactable) => OnHighlight?.Invoke(interactable);
    protected override void UnHighlight(Pressable interactable) => OnUnHighlight?.Invoke(interactable);
}
