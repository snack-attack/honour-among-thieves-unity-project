﻿using System;
using UnityEngine;
using UltEvents;

/// <summary> Vector2: dashForce </summary>
[Serializable] public class DashUltEvent : UltEvent<Vector3> { }

/// <summary> float: stunDuration </summary>
[Serializable] public class StunnableUltEvent : UltEvent<float> { }

/// <summary> float: stunDuration, Vector3: stunPosition </summary>
[Serializable] public class StunnerUltEvent : UltEvent<float, Vector3> { }

/// <summary> float: speedPercent </summary>
[Serializable] public class SpeedAffectedUltEvent : UltEvent<float> { }

/// <summary> Vector3: speedPosition </summary>
[Serializable] public class SpeedableUltEvent : UltEvent<Vector3> { }

/// <summary> float: speedPercent, Vector3: speedPosition </summary>
[Serializable] public class SpeederUltEvent : UltEvent<float, Vector3> { }

/// <summary> Vector3: pushPosition, Vector3: pushForce </summary>
[Serializable] public class PusherUltEvent : UltEvent<Vector3, Vector3> { }

/// <summary> Vector3: pushPosition, Vector3: pushForce </summary>
[Serializable] public class PushableUltEvent : UltEvent<Vector3, Vector3> { }

/// <summary> Item: item </summary>
[Serializable] public class ItemInteractUltEvent : UltEvent<Item> { }


/// <summary> Pressable: pressable </summary>
[Serializable] public class PressableInteractUltEvent : UltEvent<Pressable> { }


/// <summary> bool: active </summary>
[Serializable] public class PressableSwitchUltEvent : UltEvent<bool> { }


/// <summary> Vector3: disabledPosition </summary>
[Serializable] public class DashDisablerUltEvent : UltEvent<Vector3> { }

/// <summary> LootInfo: lootInfo </summary>
[Serializable] public class LootInfoUltEvent : UltEvent<LootInfo> { }

/// <summary> LootInfo: lootItem, Vector3: lootPosition </summary>
[Serializable] public class LootUltEvent : UltEvent<LootInfo, Vector3> { }

/// <summary> string: victoryMessage, Color: victoryColor </summary>
[Serializable] public class WinUltEvent : UltEvent<string, Color> { }

/// <summary> Vector3: breakPoint </summary>
[Serializable] public class BreakableItemUltEvent : UltEvent<Vector3> { }


[Serializable] public class FloatUltEvent : UltEvent<float> { }