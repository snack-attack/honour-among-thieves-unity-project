﻿using UltEvents;
using UnityEngine;



public class Item : MonoBehaviour, IInteractable
{
    [SerializeField] private Rigidbody _itemBody = default;
    [SerializeField] private Collider _interactableCollider = default;

    [SerializeField] protected UltEvent OnPickup = default;
    [SerializeField] protected UltEvent OnHeld = default;
    [SerializeField] protected UltEvent OnDisarm = default;
    [SerializeField] protected UltEvent OnDrop = default;
    [SerializeField] protected UltEvent OnThrow = default;

    [SerializeField] private UltEvent _onInteractEvent = default;
    [SerializeField] private BoolUltEvent _onChangeHighlightEvent = default;
    [SerializeField] private UltEvent _onHighlightEvent = default;
    [SerializeField] private UltEvent _onUnHighlightEvent = default;


    public Rigidbody ItemBody => _itemBody;
    public Collider InteractableCollider => _interactableCollider;

    public UltEvent OnInteractEvent => _onInteractEvent;
    public BoolUltEvent OnChangeHighlightEvent => _onChangeHighlightEvent;
    public UltEvent OnHighlightEvent => _onHighlightEvent;
    public UltEvent OnUnHighlightEvent => _onUnHighlightEvent;

    public Transform ItemTransform => _itemBody.transform;

    public bool CanInteract { get; set; } = true;



    public void PickedUp(ItemHolder itemHolder)
    {
        CanInteract = false;
        OnPickup?.Invoke();
        OnPickedUp(itemHolder);
    }
    protected virtual void OnPickedUp(ItemHolder itemHolder) { }


    public void Held(ItemHolder itemHolder)
    {
        OnHeld?.Invoke();
        OnHeldUpdate(itemHolder);
    }
    protected virtual void OnHeldUpdate(ItemHolder itemHolder) { }


    public void Disarmed(ItemHolder itemHolder)
    {
        CanInteract = true;
        OnDisarm?.Invoke();
        OnDisarmed(itemHolder);
    }
    protected virtual void OnDisarmed(ItemHolder itemHolder) { }

    public void Dropped(ItemHolder itemHolder)
    {
        OnDrop?.Invoke();
        OnDropped(itemHolder);
    }
    protected virtual void OnDropped(ItemHolder itemHolder) { }

    public void Thrown(ItemHolder itemHolder)
    {
        OnThrow?.Invoke();
        OnThrown(itemHolder);
    }
    protected virtual void OnThrown(ItemHolder itemHolder) { }
}