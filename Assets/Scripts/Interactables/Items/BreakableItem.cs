﻿using UnityEngine;


public class BreakableItem : Item
{
    [SerializeField] private GameObject _spawnPrefab = default;
    [SerializeField] private Collider[] _breakableItemColliders = default;
    [SerializeField] private BreakableItemUltEvent OnBreak = default;

    private bool _canBreak = false;

    protected override void OnDisarmed(ItemHolder itemHolder)
    {
        _canBreak = true;
        foreach (Collider breakableItemCollider in _breakableItemColliders)
            Physics.IgnoreCollision(breakableItemCollider, itemHolder.HoldCollider);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!_canBreak)
            return;


        Vector3 collisionPoint = collision.GetAverageCollisionPoint();
        Vector3 spawnPosition = collisionPoint;
        RaycastHit hit;
        LayerMask floorMask = LayerMask.GetMask("Floor");
        if (Physics.Raycast(collisionPoint, Vector3.down, out hit, 10f, floorMask))
            spawnPosition = hit.point;

        Instantiate(_spawnPrefab, spawnPosition, Quaternion.identity);
        OnBreak?.Invoke(collision.GetAverageCollisionPoint());
        Destroy(ItemBody.gameObject);
    }
}