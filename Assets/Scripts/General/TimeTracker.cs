﻿using UnityEngine;


public class TimeTracker
{
    public float Duration { get; private set; }
    public float StartTime { get; private set; }
    public float Progress => Duration == 0.0f ? 1.0f : TimePassed / Duration;
    public bool Stopped => TimePassed >= Duration;
    public float TimePassed
    {
        get
        {
            float actualTime = Time.time - StartTime;
            float clampedTime = Mathf.Clamp(actualTime, 0.0f, Duration);
            return clampedTime;
        }
    }


    public void Set(float duration)
    {
        Duration = duration;
        StartTime = Time.time;
    }
}