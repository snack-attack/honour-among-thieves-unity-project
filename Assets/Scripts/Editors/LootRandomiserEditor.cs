﻿using UnityEditor;
using UnityEngine;


#if UNITY_EDITOR

[CustomEditor(typeof(LootRandomiser))]
public class LootRandomiserEditor : Editor
{
    private SerializedProperty _lootPoolProperty = default;
    private SerializedProperty _lootZonesProperty = default;
    private SerializedProperty _lootCountProperty = default;


    private int TotalLootCount => _lootZonesProperty.arraySize * _lootCountProperty.intValue;


    private void OnEnable()
    {
        _lootPoolProperty = serializedObject.FindProperty("_lootPool");
        _lootZonesProperty = serializedObject.FindProperty("_lootZones");
        _lootCountProperty = serializedObject.FindProperty("_lootCount");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();
        DoLootCountWarningMessage();
        serializedObject.ApplyModifiedProperties();
    }

    private void DoLootCountWarningMessage()
    {
        if (_lootPoolProperty == null || _lootPoolProperty.objectReferenceValue == null)
            return;

        int lootPoolItemCount = new SerializedObject(_lootPoolProperty.objectReferenceValue).FindProperty("_lootPoolItems").arraySize;

        if (lootPoolItemCount < TotalLootCount)
        {
            Color originalGUIColor = GUI.color;
            GUI.color = Color.red;
            EditorGUILayout.LabelField("Not enough Loot Items in Loot Pool!");
            GUI.color = originalGUIColor;
        }
    }
}

#endif