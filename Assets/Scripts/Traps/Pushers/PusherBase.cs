﻿using System.Collections.Generic;
using UnityEngine;


public abstract class PusherBase : Affector<Rigidbody>
{
    [SerializeField] private float _pushStrength = default;
    [SerializeField] private Vector3 _relativePushDirection = default;

    [SerializeField] private PusherUltEvent OnPush = default;

    private Vector3 WorldPushDirection => transform.TransformDirection(_relativePushDirection);
    private Vector3 PushForce => _pushStrength * WorldPushDirection;


    protected override void Affect(Rigidbody pushBody)
    {
        if (pushBody == null)
            return;

        pushBody.velocity = PushForce;
        OnPush?.Invoke(pushBody.position, PushForce);
    }
}
