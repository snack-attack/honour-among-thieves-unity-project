﻿using UltEvents;
using UnityEngine;



public class ItemHolder : MonoBehaviour
{
    [SerializeField] private Transform _holdTransform = default;
    [SerializeField] private Collider _holdCollider = default;
    [SerializeField] private float _throwForce = default;

    [SerializeField] private UltEvent OnItemPickup = default;
    [SerializeField] private UltEvent OnItemHeld = default;
    [SerializeField] private UltEvent OnItemDisarmed = default;
    [SerializeField] private UltEvent OnItemDropped = default;
    [SerializeField] private UltEvent OnItemThrown = default;

    private Item _heldItem = default;

    public Collider HoldCollider => _holdCollider;


    public void SwapItem(Item item)
    {
        DropItem();
        PickupItem(item);
    }


    private void PickupItem(Item item)
    {
        if (item == null || !item.isActiveAndEnabled)
            return;

        _heldItem = item;

        _heldItem.ItemTransform.SetParent(_holdTransform);
        _heldItem.ItemTransform.localPosition = Vector3.zero;
        _heldItem.ItemTransform.localRotation = Quaternion.identity;
        _heldItem.ItemBody.isKinematic = true;

        _heldItem.PickedUp(this);
        OnItemPickup?.Invoke();
    }


    public void Disarm()
    {
        if (_heldItem == null)
            return;

        _heldItem.ItemTransform.SetParent(null);
        _heldItem.ItemBody.isKinematic = false;

        _heldItem.Disarmed(this);
        OnItemDisarmed?.Invoke();

        _heldItem = null;
    }


    public void DropItem()
    {
        if (_heldItem == null)
            return;

        _heldItem.Dropped(this);
        OnItemDropped?.Invoke();

        Disarm();
    }


    private void FixedUpdate()
    {
        if (_heldItem == null)
            return;

        if (_heldItem.isActiveAndEnabled)
        {
            _heldItem.Held(this);
            OnItemHeld?.Invoke();
        }
        else
            Disarm();
    }


    private void OnThrow() => ThrowItem(_throwForce, transform.forward);

    public void ThrowItem(float throwForce, Vector3 throwDirection)
    {
        if (_heldItem == null)
            return;

        _heldItem.Thrown(this);
        OnItemThrown?.Invoke();

        Item tempHeldItem = _heldItem;
        Disarm();
        tempHeldItem.ItemBody.AddForce(throwForce * throwDirection, ForceMode.Impulse);
    }
}