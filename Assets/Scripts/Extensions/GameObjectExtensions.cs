﻿using System;
using System.Collections.Generic;
using UnityEngine;



[Flags]
public enum eComponentRelativeType
{
    SELF = 1,
    CHILDREN = 2,
    PARENT = 4
};

public static class GameObjectExtensions
{
    public static T GetComponentRelative<T>(this GameObject gameObject, eComponentRelativeType relativeType, bool includeInactive)
    {
        T component;
        if (relativeType.HasFlag(eComponentRelativeType.SELF) && (component = gameObject.GetComponent<T>()) != null)
            return component;
        if (relativeType.HasFlag(eComponentRelativeType.CHILDREN) && (component = gameObject.GetComponentInChildren<T>(includeInactive)) != null)
            return component;
        if (relativeType.HasFlag(eComponentRelativeType.PARENT) && (component = gameObject.GetComponentInParent<T>()) != null)
            return component;
        return default;
    }

    public static T[] GetComponentsRelative<T>(this GameObject gameObject, eComponentRelativeType relativeType, bool includeInactive)
    {
        List<T> components = new List<T>();

        if (relativeType.HasFlag(eComponentRelativeType.SELF))
            components.AddRange(gameObject.GetComponents<T>());
        if (relativeType.HasFlag(eComponentRelativeType.CHILDREN))
            components.AddRange(gameObject.GetComponentsInChildren<T>(includeInactive));
        if (relativeType.HasFlag(eComponentRelativeType.PARENT))
            components.AddRange(gameObject.GetComponentsInParent<T>());

        return components.ToArray();
    }
}