﻿using UltEvents;
using UnityEngine;


public class PressableButton : Pressable
{
    [SerializeField] private UltEvent OnPressed = default;

    public override void Press() => OnPressed.Invoke();
}