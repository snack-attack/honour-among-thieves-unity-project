﻿using System.Collections.Generic;
using UnityEngine;


public class LootRandomiser : MonoBehaviour
{
    [SerializeField] private LootPool _lootPool = default;
    [SerializeField] private LootZone[] _lootZones = default;

    [SerializeField] private int _lootCount = default;


    private void Start()
    {
        List<List<LootItem>> lootLists = new List<List<LootItem>>();
        _lootPool.GetInstantiatedLootLists(_lootCount, _lootZones.Length, lootLists);

        for (int i = 0; i < _lootZones.Length; ++i)
            foreach (LootItem lootItem in lootLists[i])
                _lootZones[i].AddLootInfo(lootItem.LootInfo);
    }
}