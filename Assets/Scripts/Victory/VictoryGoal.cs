﻿using UnityEngine;




public class VictoryGoal : MonoBehaviour
{
    [SerializeField] private WinUltEvent OnWin = default;

    private void OnTriggerEnter(Collider other)
    {
        VictoryCondition victoryCondition = other.gameObject.GetComponentRelative<VictoryCondition>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, false);
        if (victoryCondition == null)
            return;

        OnWin?.Invoke(victoryCondition.VictoryMessage, victoryCondition.VictoryColor);
    }
}
