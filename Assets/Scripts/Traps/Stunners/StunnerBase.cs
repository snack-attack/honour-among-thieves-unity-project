﻿using UnityEngine;


public abstract class StunnerBase : Affector<Stunnable>
{
    [SerializeField] private eStunType _stunType = default;
    [SerializeField] private float _stunDuration = default;
    [SerializeField] private StunnerUltEvent OnStun = default;
    private Vector3 _stunNormal = default;

    protected abstract float RotationTime { get; }

    protected void StoreStunNormal(Vector3 stunNormal) => _stunNormal = stunNormal;

    protected override void Affect(Stunnable stunnable)
    {
        if (stunnable == null)
            return;

        stunnable.StunAlongNormal(_stunDuration, _stunNormal, RotationTime, _stunType);
        OnStun?.Invoke(_stunDuration, stunnable.StunBody.position);
    }
}
