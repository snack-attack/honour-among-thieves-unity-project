﻿using UnityEngine;


public class ItemInteractor : Interactor<Item>
{
    [SerializeField] private ItemInteractUltEvent OnInteract = default;
    [SerializeField] private ItemInteractUltEvent OnHighlight = default;
    [SerializeField] private ItemInteractUltEvent OnUnHighlight = default;


    protected override void Interact(Item item) => OnInteract?.Invoke(item);
    protected override void Highlight(Item item) => OnHighlight?.Invoke(item);
    protected override void UnHighlight(Item item) => OnUnHighlight?.Invoke(item);
}
