﻿using System.Collections;
using UltEvents;
using UnityEngine;


public enum eStunType { KNOCKBACK, IMMOBOLIZE };


public class Stunnable : MonoBehaviour
{
    [SerializeField] private Rigidbody _stunBody = default;

    [SerializeField] private StunnableUltEvent OnKnockbackStart = default;
    [SerializeField] private StunnableUltEvent OnKnockbackUpdate = default;
    [SerializeField] private UltEvent OnKnockbackEnd = default;

    [SerializeField] private StunnableUltEvent OnImmobolizeStart = default;
    [SerializeField] private StunnableUltEvent OnImmobolizeUpdate = default;
    [SerializeField] private UltEvent OnImmobolizeEnd = default;

    [SerializeField] private StunnableUltEvent OnStunStart = default;
    [SerializeField] private StunnableUltEvent OnStunUpdate = default;
    [SerializeField] private UltEvent OnStunEnd = default;

    private bool _isStunned = default;

    public Rigidbody StunBody => _stunBody;


    public void Stun(float duration, eStunType stunType)
    {
        if (!_isStunned)
            StartCoroutine(StunRoutine(duration, _stunBody.rotation, 0.0f, stunType));
    }

    public void StunAlongNormal(float duration, Vector3 stunNormal, float rotationTime, eStunType stunType)
    {
        if (!_isStunned)
        {
            Quaternion stunRotation = Quaternion.Euler(0.0f, Quaternion.LookRotation(stunNormal, Vector3.up).eulerAngles.y, 0.0f);
            StartCoroutine(StunRoutine(duration, stunRotation, rotationTime, stunType));
        }
    }


    private IEnumerator StunRoutine(float duration, Quaternion stunRotation, float rotationTime, eStunType stunType)
    {
        StartStun(duration, stunType);

        TimeTracker stunTimer = new TimeTracker();
        stunTimer.Set(duration);

        Quaternion initialRotation = _stunBody.rotation;

        while (!stunTimer.Stopped)
        {
            UpdateStun(stunTimer, initialRotation, stunRotation, rotationTime, stunType);
            yield return null;
        }

        EndStun(stunType);
    }

    private void StartStun(float duration, eStunType stunType)
    {
        _isStunned = true;

        OnStunStart?.Invoke(duration);

        if (stunType == eStunType.KNOCKBACK) OnKnockbackStart?.Invoke(duration);
        else if (stunType == eStunType.IMMOBOLIZE) OnImmobolizeStart?.Invoke(duration);
    }

    private void UpdateStun(TimeTracker stunTimer, Quaternion initialRotation, Quaternion endRotation, float rotationTime, eStunType stunType)
    {
        TweenScaleFunctions.SineEaseInOut.Invoke(stunTimer.Progress);
        float lerpTime = rotationTime > 0.0f ? Mathf.Clamp01(stunTimer.TimePassed / rotationTime) : 1.0f;
        _stunBody.rotation = Quaternion.Slerp(initialRotation, endRotation, lerpTime);

        OnStunUpdate?.Invoke(stunTimer.Progress);
        if (stunType == eStunType.KNOCKBACK) OnKnockbackUpdate?.Invoke(stunTimer.Progress);
        else if (stunType == eStunType.IMMOBOLIZE) OnImmobolizeUpdate?.Invoke(stunTimer.Progress);
    }

    private void EndStun(eStunType stunType)
    {
        _isStunned = false;

        OnStunEnd?.Invoke();
        if (stunType == eStunType.KNOCKBACK) OnKnockbackEnd?.Invoke();
        else if (stunType == eStunType.IMMOBOLIZE) OnImmobolizeEnd?.Invoke();
    }
}
