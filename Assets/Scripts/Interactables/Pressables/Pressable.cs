﻿using UltEvents;
using UnityEngine;


public abstract class Pressable : MonoBehaviour, IInteractable
{
    [SerializeField] private Collider _interactableCollider = default;

    [SerializeField] private UltEvent _onInteractEvent = default;
    [SerializeField] private BoolUltEvent _onChangeHighlightEvent = default;
    [SerializeField] private UltEvent _onHighlightEvent = default;
    [SerializeField] private UltEvent _onUnHighlightEvent = default;

    public Collider InteractableCollider => _interactableCollider;
    public bool CanInteract { get; set; } = true;

    public UltEvent OnInteractEvent => _onInteractEvent;
    public BoolUltEvent OnChangeHighlightEvent => _onChangeHighlightEvent;
    public UltEvent OnHighlightEvent => _onHighlightEvent;
    public UltEvent OnUnHighlightEvent => _onUnHighlightEvent;

    public abstract void Press();
}