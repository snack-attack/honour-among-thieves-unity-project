﻿using UnityEngine;



[CreateAssetMenu(menuName = "SnackAttack/LootInfo")]
public class LootInfo : ScriptableObject
{
    [SerializeField] private string _lootName = default;
    [SerializeField] private Sprite _lootSprite = default;

    public string LootName => _lootName;
    public Sprite LootSprite => _lootSprite;
}