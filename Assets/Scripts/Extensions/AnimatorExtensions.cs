﻿using UnityEngine;

public static class AnimatorExtensions
{
    public static void SetInverseFloat(this Animator animator, string floatName, float value) => animator.SetFloat(floatName, 1 / value);
}