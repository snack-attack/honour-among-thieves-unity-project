﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;


public class UnObscurer : MonoBehaviour
{
    private class FadeInfo
    {
        public Renderer[] Renderers { get; set; }
        public float FadeAlpha { get; set; }

        public FadeInfo(Renderer[] renderers, float fadeAlpha)
        {
            Renderers = renderers;
            FadeAlpha = fadeAlpha;
        }
    }



    [SerializeField] private List<Collider> _targets = default;
    [SerializeField] private float _fadeTime = default;
    [SerializeField][Range(0, 1)] private float _fadePercent = default;

    private Camera _mainCamera = default;
    private readonly Dictionary<GameObject, FadeInfo> _fadedObjects = new Dictionary<GameObject, FadeInfo>();


    private void Start() => _mainCamera = Camera.main;

    void Update()
    {
        List<RaycastHit> hits = new List<RaycastHit>();
        Vector3 origin = _mainCamera.transform.position;

        foreach (Collider target in _targets)
        {
            Vector3 deltaPos = target.transform.position - origin;
            Vector3 direction = deltaPos.normalized;

            RaycastHit[] raycastHits = Physics.RaycastAll(origin, direction, deltaPos.magnitude);
            hits.AddRange(raycastHits);
        }

        ProcessRaycastHits(hits);
    }


    private void ProcessRaycastHits(List<RaycastHit> raycastHits)
    {
        List<GameObject> newObjects = new List<GameObject>();

        foreach (RaycastHit raycastHit in raycastHits)
        {
            if (_targets.Contains(raycastHit.collider))
                continue;

            if (!raycastHit.collider.CompareTag("Hideable"))
                continue;

            newObjects.Add(raycastHit.collider.gameObject);
        }

        ProcessNewObjects(newObjects);
    }

    private void ProcessNewObjects(List<GameObject> newObjects)
    {
        // Fade new objects that have not been faded.
        foreach (GameObject newObject in newObjects)
            if (!_fadedObjects.ContainsKey(newObject))
                FadeObject(newObject);

        // Un Fade objects not hit.
        foreach (GameObject fadedObject in _fadedObjects.Keys.ToArray())
            if (!newObjects.Contains(fadedObject))
                UnFadeObject(fadedObject);
    }


    private void FadeObject(GameObject objectToFade)
    {
        Renderer[] renderers = objectToFade.gameObject.GetComponentsRelative<Renderer>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, false);
        _fadedObjects.Add(objectToFade, new FadeInfo(renderers, 1));

        foreach (Renderer renderer in renderers)
            StartCoroutine(FadeRenderer(renderer, _fadedObjects[objectToFade].FadeAlpha, _fadePercent));

        _fadedObjects[objectToFade].FadeAlpha = _fadePercent;
    }

    private void UnFadeObject(GameObject objectToUnFade)
    {
        foreach (Renderer renderer in _fadedObjects[objectToUnFade].Renderers)
            StartCoroutine(FadeRenderer(renderer, _fadedObjects[objectToUnFade].FadeAlpha, 1 / _fadePercent));

        _fadedObjects.Remove(objectToUnFade);
    }


    private IEnumerator FadeRenderer(Renderer renderer, float startAlpha, float fadePercent)
    {
        if (renderer == null || !renderer.material.HasProperty("Alpha_Mult"))
            yield break;

        TimeTracker fadeTimer = new TimeTracker();
        fadeTimer.Set(_fadeTime);

        float endAlpha = startAlpha * fadePercent;
        float changeAlpha = endAlpha - startAlpha;

        float previousAlphaChange = 0;

        do
        {
            yield return null;

            float tweenValue = TweenScaleFunctions.SineEaseOut(fadeTimer.Progress);

            float currentAlpha = renderer.material.GetFloat("Alpha_Mult");
            float currentAlphaChange = tweenValue * changeAlpha;
            float alphaStep = currentAlphaChange - previousAlphaChange;

            renderer.material.SetFloat("Alpha_Mult", currentAlpha + alphaStep);

            previousAlphaChange = currentAlphaChange;
        } while (!fadeTimer.Stopped);
    }
}
