﻿using UnityEngine;

public class VictoryCondition : MonoBehaviour
{
    [SerializeField] private string _victoryMessage = default;
    [SerializeField] private Color _victoryColor = default;

    public string VictoryMessage => _victoryMessage;
    public Color VictoryColor => _victoryColor;
}
